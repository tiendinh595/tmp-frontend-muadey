const express = require('express');
const app = express();
const path = require('path');
const request = require('request');
let NODE_ENV = process.env.NODE_ENV;
const API_URI = NODE_ENV === 'development' ? 'http://127.0.0.1:8000' : 'https://api.muadey.com';
const DOMAIN = 'https://muadey.com';
app.set('view engine', 'ejs');
app.set('views', path.resolve(__dirname));
app.use('/dist', express.static('dist'));
app.use('/assets', express.static('assets'));
//description: 'Mua bán nhà đất',
// keywords: 'Mua bán nhà đất'
function getHead(title = 'Mua bán nhà đất',
                 meta = [
                     {
                         type: 'name',
                         name: 'description',
                         content: 'Kênh thông tin số 1 về bất động sản tại Việt Nam: mua bán nhà đất, cho thuê nhà đất, văn phòng, căn hộ, biệt thự, chung cư. Các lĩnh vực liên quan đến bất động sản: xây dựng, nội thất, kiến trúc, ngoại thất, phong thuỷ, luật pháp, tư vấn.',
                     }, {
                         type: 'name',
                         name: 'keywords',
                         content: 'Kênh, thông, tin, số, 1, về, bất, động, sản',
                     }, {
                         type: 'property',
                         name: 'og:url',
                         content: DOMAIN,
                     }, {
                         type: 'property',
                         name: 'og:type',
                         content: 'article',
                     }, {
                         type: 'property',
                         name: 'og:title',
                         content: 'Nhà đất | Mua bán nhà đất | Cho thuê nhà đất'
                     }, {
                         type: 'property',
                         name: 'og:description',
                         content: 'Kênh thông tin số 1 về bất động sản tại Việt Nam: mua bán nhà đất, cho thuê nhà đất, văn phòng, căn hộ, biệt thự, chung cư. Các lĩnh vực liên quan đến bất động sản: xây dựng, nội thất, kiến trúc, ngoại thất, phong thuỷ, luật pháp, tư vấn.',
                     }
                 ]) {
    let meta_tags = meta.map(item => {
        return `<meta ${item.type}="${item.name}" content="${item.content}" />`;
    });
    return `
    <title>${title}</title>
    ${meta_tags.join('')}
    `;
}

app.get('/:slug-pr:id', (req, res) => {
    request(`${API_URI}/products/${req.params.id}`, function (err, response, body) {
        if (!err && response.statusCode === 200) {
            const data = JSON.parse(body);
            if (data.code === 200) {
                const product = data.data;
                let image = null;

                try {
                    image = product.images[0].full_url;
                } catch (err) {
                    image = `${API_URI}/storage/images/default-image.png`;
                }

                let meta = [
                        {
                            type: 'name',
                            name: 'description',
                            content: product.description.slice(0, 255),
                        }, {
                            type: 'name',
                            name: 'keywords',
                            content: product.title,
                        }, {
                            type: 'property',
                            name: 'og:url',
                            content: `${DOMAIN}${req.url}`,
                        }, {
                            type: 'property',
                            name: 'og:type',
                            content: 'article',
                        }, {
                            type: 'property',
                            name: 'og:title',
                            content: product.title,
                        }, {
                            type: 'property',
                            name: 'og:description',
                            content: product.description.slice(0, 255),
                        }, {
                            type: 'property',
                            name: 'og:image',
                            content: image
                        }
                    ]
                ;
                return res.render('index', {
                    head: getHead(data.data.title, meta)
                });
            }
        }
        return res.redirect('/404')
    });
});

app.get('/*', (req, res) => {
    console.log(req.url);
    res.render('index', {
        head: getHead()
    });
});

app.listen(3001, () => console.log('app running on port 3001!'));