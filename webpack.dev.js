const merge = require('webpack-merge');
const common = require('./webpack.config.js');

module.exports = merge(common, {
    mode: "development",
    devtool: "source-map",
    devServer: {
        historyApiFallback: true,
        publicPath: "/dist",
        compress: true,
        port: 9000
    },
});