import {observable, computed, action} from 'mobx'

class CommonStore {

    @observable token = window.localStorage.getItem('jwt');

    @observable app_loaded = false;

    @observable previous_url = window.location.pathname;
    @observable current_url = window.location.pathname;

    @action
    setToken(token) {
        this.token = token;
        window.localStorage.setItem('jwt', token)
    }

    @action
    removeToken() {
        this.token = null;
        window.localStorage.removeItem('jwt')
    }

    @action
    setAppLoaded() {
        this.app_loaded = true;
    }

    @action
    setPreviousUrl(url) {
        this.previous_url = url;
    }

    @action
    setCurrentUrl(url) {
        this.setPreviousUrl(this.current_url);
        this.current_url = url;
    }

    @computed get get_current_url() {
        return this.current_url;
    }
}

export default new CommonStore()