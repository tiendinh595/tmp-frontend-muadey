import React, {Fragment} from 'react'
import BaseComponent from 'Components/BaseComponent'
import WindowTitle from 'Components/Layout/WindowTitle'
import MasterLayout from 'Components/Layout/MasterLayout'
import * as ApiCaller from 'Utils/ApiCaller'
import {Redirect} from 'react-router-dom'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import FormSearch from 'Components/Common/FormSearch'
import Breadcrumb from 'Components/Common/Breadcrumb'
import ProductList from 'Components/Product/ProductList'
import ReactPaginate from 'react-paginate';
import {inject, observer, withRouter} from 'mobx-react'
import LoadingSpinner from 'loading-spinner-reactjs'


@inject('commonStore')
@observer
export default class ProductType extends BaseComponent {

    constructor(props) {
        super(props);
        this.url_api = null;
        this.state = {
            is_loading: true,
            is_load_products: true,
            category: null,
            products: [],
            meta_data: {}
        }
    }

    loadProducts = (url) => {
        this.setState({
            is_load_products: true
        });

        ApiCaller.get(url)
            .then(res => {
                if (res.code === 200)
                    this.setState({
                        is_loading: false,
                        products: res.data,
                        meta_data: res.metadata
                    });
            })
            .catch((err) => {
                console.log('er')
                this.setState({
                    is_load_products: false
                })
            })
            .finally(() => {
                console.log('er');

                this.setState({
                    is_load_products: false
                });
            });
    };

    async componentWillMount() {
        this.preloadData(this.props);
    }

    preloadData = async (props) => {
        const {id} = props.match.params;
        let category = await ApiCaller.get(`/product-types/${id}`);
        category = category.data;
        this.setState({
            category,
            is_loading: false
        });
        this.url_api = category.master === null ? `/products/?product_type_master_id=${category.id}&limit=12` : `/products/?product_type_child_id=${category.id}&limit=12`;
        this.loadProducts(this.url_api);
    };

    componentWillReceiveProps(newProps) {
        if (this.props.match.params.id !== newProps.match.params.id) {
            this.props.commonStore.setCurrentUrl(window.location.pathname);
            this.preloadData(newProps);
        }
    }

    handlePageClick = (data) => {
        let url = `${this.url_api}&page=${data.selected + 1}`;
        this.loadProducts(url);
    };

    getDataBreadcrumb = (category) => {
        let result = [];

        if (category.master !== null) {
            result.push({
                link: `/mua-ban/${category.master.id}/${category.master.slug}`,
                label: category.master.name
            })
        }
        result.push({
            link: `/mua-ban/${category.id}/${category.slug}`,
            label: category.name
        });
        return result;
    };

    render() {
        const {is_loading, products, category, is_load_products} = this.state;

        if (is_loading === false && Object.keys(category).length === 0)
            return <Redirect to="/404"/>;


        return (
            <MasterLayout>
                <NotificationContainer/>
                {
                    (() => {
                        if (!is_loading) {
                            return (
                                <Fragment>
                                    <WindowTitle title={category.name}/>
                                    <Breadcrumb links={this.getDataBreadcrumb(category)}/>
                                </Fragment>
                            )
                        }
                    })()
                }

                {/*<FormSearch is_redirect={true}/>*/}
                <div className="nhadatban_content">
                    <div className="container">

                        {
                            (() => {
                                if (!is_load_products) {
                                    return (
                                        <div className="row">
                                            <ProductList type="grid" products={products}/>
                                        </div>
                                    )
                                } else {
                                    return (
                                        <LoadingSpinner/>
                                    )
                                }
                            })()
                        }
                        {
                            (() => {
                                if (products.length !== 0) {
                                    return (
                                        <div className="row">
                                            <div className="pagination_cate text-center w-100">
                                                <nav aria-label="pagination ">
                                                    <ReactPaginate previousLabel={"«"}
                                                                   previousClassName="page-item"
                                                                   nextClassName="page-item"
                                                                   previousLinkClassName="page-link"
                                                                   nextLinkClassName="page-link"
                                                                   nextLabel={"»"}
                                                                   breakLabel={<a href="">...</a>}
                                                                   breakClassName={"break-me"}
                                                                   pageCount={this.state.meta_data.total_pages}
                                                                   marginPagesDisplayed={2}
                                                                   pageRangeDisplayed={5}
                                                                   onPageChange={this.handlePageClick}
                                                                   containerClassName={"pagination justify-content-center"}
                                                                   subContainerClassName={"pages pagination"}
                                                                   activeClassName={"active"}
                                                                   pageClassName="page-item"
                                                                   pageLinkClassName="page-link"
                                                    />
                                                </nav>
                                            </div>
                                        </div>
                                    )
                                }
                            })()
                        }

                    </div>
                </div>


            </MasterLayout>
        )
    }

}