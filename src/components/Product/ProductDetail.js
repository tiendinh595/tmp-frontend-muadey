import React, {Fragment} from 'react'
import BaseComponent from 'Components/BaseComponent'
import WindowTitle from 'Components/Layout/WindowTitle'
import MasterLayout from 'Components/Layout/MasterLayout'
import * as ApiCaller from 'Utils/ApiCaller'
import {Redirect} from 'react-router-dom'
import {withScriptjs, withGoogleMap, GoogleMap, Marker} from "react-google-maps"
import LoadingSpinner from 'loading-spinner-reactjs'
import ProductItem from 'Components/Product/ProductItem'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import {number_format} from 'Utils/Helper'
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import 'Styles/product.css'
import {inject, observer} from 'mobx-react'
import Slider from "react-slick";

const MyMapComponent = withScriptjs(withGoogleMap((props) => {
    return <GoogleMap
        defaultZoom={15}
        defaultCenter={{lat: props.lat, lng: props.lng}}
    >
        {props.isMarkerShown && <Marker position={{lat: props.lat, lng: props.lng}}/>}
    </GoogleMap>
}));

@observer
@inject('userStore')
export default class ProductDetail extends BaseComponent {

    constructor(props) {
        super(props);
        this.slide_show = null;
        this.state = {
            product: {},
            product_relates: [],
            is_loading: true,
            lightbox: {
                is_open: false,
                photo_index: 0
            },
            lightbox_extra: {
                is_open: false,
                photo_index: 0
            },

            mail_contact: {
                email: '',
                content: '',
                name: ''
            }
        }
    }

    loadData = async (id) => {

        await ApiCaller.get(`/products/${id}`)
            .then(res => {
                if (res.code === 200)
                    this.setState({
                        is_loading: false,
                        product: res.data
                    });
            });

        await ApiCaller.get(`/products/${id}/relate`)
            .then(res => {
                if (res.code === 200)
                    this.setState({
                        product_relates: res.data
                    });
            });
    };

    onChangeFormMail = (e) => {
        const {name, value} = e.target;
        this.setState({
            mail_contact: {
                ...this.state.mail_contact,
                [name]: value
            }
        })
    };


    onSendMailContact = (e) => {
        e.preventDefault();
        ApiCaller.post('/mail', this.state.mail_contact)
            .then(res => {
                if (res.code === 200) {
                    NotificationManager.success(res.message)
                    this.setState({
                        mail_contact: {
                            email: '',
                            content: '',
                            name: ''
                        }
                    })
                } else {
                    NotificationManager.error(res.message)
                }
            })
    };

    componentWillMount() {
        const {id} = this.props.match.params;
        this.loadData(id);
    }

    componentDidMount() {
        $(document).ready(() => {
            // $('#carousel-thumb').carousel('prev');
            // this.slide_show.load("/assets/mdb-addons/mdb-lightbox-ui.html");
        });
    }

    componentWillReceiveProps(newProps) {
        if (this.props.match.params.id !== newProps.match.params.id)
            this.loadData(newProps.match.params.id)
    }

    onOpenLightBox = (e) => {
        e.preventDefault();
        this.setState({lightbox: {...this.state.lightbox, is_open: true}})
    };

    onSelectImage = (e, index) => {
        this.setState({
            lightbox: {
                ...this.state.lightbox,
                photo_index: index
            }
        })
    };
    onOpenLightBoxExtra = (e) => {
        e.preventDefault();
        this.setState({lightbox_extra: {...this.state.lightbox_extra, is_open: true}})
    };

    onSelectImageExtra = (e, index) => {
        this.setState({
            lightbox_extra: {
                ...this.state.lightbox_extra,
                photo_index: index
            }
        })
    };

    renderSlider = (product) => {
        if (product.images.length === 0)
            return null;
        const {lightbox} = this.state;
        let images = product.images.map(item => item.full_url);
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true
        };
        return (
            <Fragment>

                <div className="mt-3">
                    {
                        lightbox.is_open
                            ? <Lightbox
                                mainSrc={images[lightbox.photo_index]}
                                nextSrc={images[(lightbox.photo_index + 1) % images.length]}
                                prevSrc={images[(lightbox.photo_index + images.length - 1) % images.length]}
                                onCloseRequest={() => this.setState({lightbox: {...this.state.lightbox, is_open: false}})}
                                onMovePrevRequest={() =>
                                    this.setState({
                                        lightbox: {
                                            ...this.state.lightbox,
                                            photo_index: (lightbox.photo_index + images.length - 1) % images.length
                                        }
                                    })
                                }
                                onMoveNextRequest={() =>
                                    this.setState({
                                        lightbox: {
                                            ...this.state.lightbox,
                                            photo_index: (lightbox.photo_index + 1) % images.length
                                        }
                                    })
                                }
                            />
                            : null
                    }

                    <Slider {...settings}>
                        {
                            product.images.map(item=>{
                                return <div key={item.id}>
                                    <img className="img_sld" src={item.full_url} alt="" onClick={this.onOpenLightBox}/>
                                </div>
                            })
                        }
                    </Slider>
                </div>


            </Fragment>

        )
    };

    renderSliderExtra = (product) => {
        if (product.images_extra.length === 0)
            return null;
        const {lightbox_extra} = this.state;
        let images = product.images_extra.map(item => item.full_url);

        return (
            <Fragment>

                <div>
                    {
                        lightbox_extra.is_open
                            ? <Lightbox
                                mainSrc={images[lightbox_extra.photo_index]}
                                nextSrc={images[(lightbox_extra.photo_index + 1) % images.length]}
                                prevSrc={images[(lightbox_extra.photo_index + images.length - 1) % images.length]}
                                onCloseRequest={() => this.setState({lightbox_extra: {...this.state.lightbox_extra, is_open: false}})}
                                onMovePrevRequest={() =>
                                    this.setState({
                                        lightbox_extra: {
                                            ...this.state.lightbox_extra,
                                            photo_index: (lightbox_extra.photo_index + images.length - 1) % images.length
                                        }
                                    })
                                }
                                onMoveNextRequest={() =>
                                    this.setState({
                                        lightbox_extra: {
                                            ...this.state.lightbox_extra,
                                            photo_index: (lightbox_extra.photo_index + 1) % images.length
                                        }
                                    })
                                }
                            />
                            : null
                    }
                </div>


            </Fragment>

        )
    };

    render() {
        const {is_loading, product, product_relates} = this.state;
        const {userStore} = this.props;

        if (is_loading === true) {
            return <LoadingSpinner/>
        }

        if (is_loading === false && Object.keys(product).length === 0)
            return <Redirect to="/404"/>;

        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true
        };
        return (
            <MasterLayout>
                <NotificationContainer/>
                <WindowTitle title={product.title ? product.title : 'loading...'}/>
                <div className="nhadatban_content">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="row">
                                <div className="col-md-8">


                                <div className="info_up mt-5">
                                    {/* info nha */}
                                    <div className="info_nha">
                                        <div className="info_nha__card">
                                            <div className="info_nha__left">
                                                <a>
                                                    <span className="icons-squa"/>{product.area} m<sup>2</sup>
                                                </a>
                                                {
                                                    product.room_number == null ? '' : <a>
                                                        <span className="icons-nem"/>{product.room_number}
                                                    </a>
                                                }

                                            </div>
                                            <div className="info_nha__right">
                                                <a> {product.price_type === 1 ? '' : number_format(product.price_to)} {product.price_type_name} </a>
                                            </div>
                                        </div>
                                    </div>

                                    {
                                        this.renderSlider(product)
                                    }
                                    <div className="content_detail">
                                        <h2>{product.title}</h2>
                                        <p>
                                            {
                                                product.description.split("\n").map((item, key) => {
                                                    return <span key={key}>{item} <br/></span>
                                                })
                                            }
                                        </p>
                                        <h2>Đặc điểm bất động sản</h2>
                                        <ul className="row list-dacdiembds">
                                            <li className="col-12 col-sm-6">
                                                <a href="#"><span className="icons-tick"/>Loại tin
                                                    rao: {product.product_type_master.name}
                                                    > {product.product_type_child.name}</a>
                                            </li>
                                            <li className="col-12 col-sm-6">
                                                <a href="#"><span className="icons-tick"/>Địa
                                                    chỉ {product.address}, {product.ward.name}, {product.district.name}, {product.city.name}
                                                </a>
                                            </li>
                                            <li className="col-12 col-sm-6">
                                                <a href="#"><span className="icons-tick"/>Mặt
                                                    tiền {product.frontage_area} (m)</a>
                                            </li>
                                            <li className="col-12 col-sm-6">
                                                <a href="#"><span className="icons-tick"/>Đường vào {product.land_with}
                                                    (m)</a>
                                            </li>
                                        </ul>
                                        <h2>Bản đồ</h2>
                                        <MyMapComponent
                                            isMarkerShown
                                            googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyA-i9ZySsOQ3WZdzkMEj8daMzHp2WTdHNI"
                                            loadingElement={<div style={{height: `100%`}}/>}
                                            containerElement={<div style={{height: `400px`}}/>}
                                            mapElement={<div style={{height: `100%`}}/>}
                                            lat={product.lat}
                                            lng={product.lng}
                                        />

                                    </div>
                                    {/* item carosel */}
                                </div>

                                </div>
                                    <div className="col-md-4">
                                        <h4 className="info_up__h search">{product.user.name} </h4>
                                        <div className="info_user">
                                            <div className="info_user__img">
                                                <img src={product.user.avatar} style={{width: '70px', height: '70px'}}/>
                                            </div>
                                            <div className="info_user__text">
                                                <p>{product.contact_name}</p>
                                            </div>
                                            <div className="info_user__contact mt-3 clearfix">
                                                <ul>
                                                    <li>
                                                        <a href={`call:${userStore.current_user === null ? '' : product.contact_phone}`}><span
                                                            className="icons-call"/>{userStore.current_user === null ? 'Đăng nhập để hiển thị' : product.contact_phone}</a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <p className="text-left"><b>Giấy tờ liên quan:</b></p>

                                            {
                                                userStore.current_user === null
                                                    ? <p className="text-left">Đăng nhập để hiển thị</p>
                                                    : <Fragment>
                                                        {this.renderSliderExtra(product)}
                                                        <Slider {...settings}>
                                                            {
                                                                product.images_extra.map(item=>{
                                                                    return <div key={item.id}>
                                                                        <img src={item.full_url} alt="" onClick={this.onOpenLightBoxExtra}/>
                                                                    </div>
                                                                })
                                                            }
                                                        </Slider>
                                                    </Fragment>
                                            }

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div className="tinlienquan">
                                            <h3>
                                                Tin liên quan
                                            </h3>
                                        </div>
                                        <div className="row mt-3">
                                            {
                                                product_relates.map(product => <ProductItem key={product.id}
                                                                                            className="col-md-6"
                                                                                            product={product}/>)
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </MasterLayout>
        )
    }

}