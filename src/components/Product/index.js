import React from 'react'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import {withScriptjs, withGoogleMap, GoogleMap, Marker} from "react-google-maps"
import {compose, withProps, lifecycle} from "recompose";
import {SearchBox} from "react-google-maps/lib/components/places/SearchBox"
import 'Styles/upload.css'
import _ from 'lodash'


export class UploadItem extends React.Component {
    state = {is_deleted: false};

    constructor(props) {
        super(props)
    }

    onDelete = () => {
        const {id} = this.props.image;
        const name = this.props.name;
        this.props.onDelete(id, name);
        this.setState({
            is_deleted: true
        });
        NotificationManager.success('xóa file thành công');


        // ApiCaller.remove(`/files/${id}`)
        //     .then(res => {
        //         if (res.code === 200) {
        //             this.setState({
        //                 is_deleted: true
        //             });
        //             NotificationManager.success('xóa file thành công');
        //             this.props.onDelete(id)
        //         }
        //         else
        //             NotificationManager.errr(res.message)
        //     })
        //     .catch(err => NotificationManager.errr('Có lỗi xảy ra'))
    };

    render() {
        const {image} = this.props;

        if (this.state.is_deleted)
            return null;

        return (
            <div className="upload-item">
                <NotificationContainer/>
                <img src={image.full_url} className="img-thumbnail"/>
                <i className="fa fa-trash" onClick={this.onDelete}/>
            </div>
        )
    }
}

export class ListImageUpload extends React.PureComponent {

    constructor(props) {
        super(props)
    }

    render() {
        const {images, name} = this.props;
        console.log(this.props)
        return (
            <div className="upload_wrap">
                {
                    images.map(image => <UploadItem key={image.id} image={image} name={this.props.name} onDelete={this.props.onDelete}/>)
                }
            </div>
        )
    }
}

export const MyMapComponent = withScriptjs(withGoogleMap((props) => {
    return <GoogleMap
        defaultZoom={15}
        defaultCenter={{lat: props.lat, lng: props.lng}}
    >
        <Marker
            position={{lat: props.lat, lng: props.lng}}
            draggable={true}
            onDragEnd={props.onChangeLocation}
        />
    </GoogleMap>
}));

export const MapWithASearchBox = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyA-i9ZySsOQ3WZdzkMEj8daMzHp2WTdHNI",
        loadingElement: <div style={{height: `100%`}}/>,
        containerElement: <div style={{height: `400px`}}/>,
        mapElement: <div style={{height: `100%`}}/>,
    }),
    lifecycle({
        componentWillMount() {
            const refs = {};

            this.setState({
                bounds: null,
                center: {
                    lat: this.props.lat, lng: this.props.lng
                },
                markers: [],
                onMapMounted: ref => {
                    refs.map = ref;
                },
                onBoundsChanged: () => {
                    this.setState({
                        bounds: refs.map.getBounds(),
                        center: refs.map.getCenter(),
                    })
                },
                onSearchBoxMounted: ref => {
                    refs.searchBox = ref;
                },
                onPlacesChanged: () => {
                    const places = refs.searchBox.getPlaces();
                    const bounds = new google.maps.LatLngBounds();

                    places.forEach(place => {
                        if (place.geometry.viewport) {
                            bounds.union(place.geometry.viewport)
                        } else {
                            bounds.extend(place.geometry.location)
                        }
                    });
                    const nextMarkers = places.map(place => ({
                        position: place.geometry.location,
                    }));
                    const nextCenter = _.get(nextMarkers, '0.position', this.state.center);
                    this.props.onChangeLocation(nextCenter.lat(), nextCenter.lng());
                    this.setState({
                        center: nextCenter,
                        markers: nextMarkers,
                    });
                    // refs.map.fitBounds(bounds);
                },
            })
        },
    }),
    withScriptjs,
    withGoogleMap
)(props =>
    <GoogleMap
        ref={props.onMapMounted}
        defaultZoom={15}
        center={props.center}
        onBoundsChanged={props.onBoundsChanged}
        defaultCenter={{lat: props.lat, lng: props.lng}}
    >
        {console.log('defaultAddress',props.defaultAddress)}
        <SearchBox
            ref={props.onSearchBoxMounted}
            bounds={props.bounds}
            controlPosition={google.maps.ControlPosition.TOP_LEFT}
            onPlacesChanged={props.onPlacesChanged}
        >
            <input
                type="text"
                placeholder="Nhập địa chỉ"
                style={{
                    boxSizing: `border-box`,
                    border: `1px solid transparent`,
                    width: `240px`,
                    height: `32px`,
                    marginTop: `10px`,
                    padding: `0 12px`,
                    borderRadius: `3px`,
                    boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                    fontSize: `14px`,
                    outline: `none`,
                    textOverflow: `ellipses`,
                }}
            />
        </SearchBox>
        <Marker
            position={{lat: props.lat, lng: props.lng}}
            draggable={true}
            onDragEnd={props.onChangeLocation}
        />
        {props.markers.map((marker, index) =>
            <Marker
                key={index}
                draggable={true}
                position={marker.position}
                onDragEnd={(e) => {
                    props.onChangeLocation(e.latLng.lat(), e.latLng.lng())
                }}/>
        )}
    </GoogleMap>
);