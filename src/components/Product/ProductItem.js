import React, {PureComponent} from 'react'
import {API_URI} from 'Constants'
import {Link} from 'react-router-dom'
import {number_format} from 'Utils/Helper'

export default class ProductItem extends PureComponent {
    constructor(props) {
        super(props)
    }

    render() {
        let {type, product, className} = this.props;

        if(className === undefined)
            className = "col-md-4";

        return (
            type === 'list'
                ?

                <div className="mb-4 card-style_list">
                    <div className="view overlay">
                        <img className="img-fluid" src={product.thumbnail ? product.thumbnail.full_url : `${API_URI}/storage/images/default-image.png`}/>
                        <Link to={`/${product.slug}-pr${product.id}`}>
                            <div className="mask rgba-white-slight"/>
                        </Link>
                    </div>
                    <div className="card_list_item">
                        <ul>
                            <li>
                                <strong>{product.title}</strong>
                            </li>
                            <li>
                                -
                                <span>Diện tích: {product.area} m<sup>2</sup></span>
                                {
                                    product.room_number === null ? '' : <span>Phòng: {product.room_number}</span>
                                }
                            </li>
                            <li>
                                - Giá {number_format(product.price_to)}
                            </li>
                            <li>
                                - Địa Chỉ: {product.address}
                            </li>
                            <li>
                                <Link to={`/${product.slug}-pr${product.id}`} title="Details" className="btn btn-primary  detail-list">Mua Ngay</Link>
                            </li>
                        </ul>
                    </div>
                </div>
                :
                <div className={className}>
                    <div className="card mb-4 card-style">
                        <div className="view overlay">
                            <img className="img-fluid" src={product.thumbnail ? product.thumbnail.full_url : `${API_URI}/storage/images/default-image.png`}/>
                            <Link to={`/${product.slug}-pr${product.id}`}>
                                <div className="mask rgba-white-slight"/>
                            </Link>
                            <span className="currentPlace">{product.district.name} - {product.city.name}</span>
                            <div className="info_house">
                                <a>
                                    <span className="icons-squa2"/>{product.area}m<sup>2</sup>
                                </a>
                                {
                                    product.room_number === null ? '' : <a>
                                        <span className="icons-nem2"/>{product.room_number}
                                    </a>
                                }
                            </div>
                        </div>
                        <div className="card-body">
                            <p className="card-text">{product.title}</p>
                        </div>
                        <div className="card-footer">
                            <a>{product.price_type === 1 ? '' : number_format(product.price_to)} { product.price_type_name}</a>
                            <Link to={`/product/${product.id}/${product.slug}`}>
                                <span className="icons-squa"/>{product.area}m<sup>2</sup></Link>
                            <Link to={`/${product.slug}-pr${product.id}`}>Mua Ngay</Link>
                        </div>
                    </div>
                </div>
        )
    }
}