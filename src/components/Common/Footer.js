import React, {Fragment} from 'react'

export default class Footer extends React.Component{
    render() {
        return (
            <Fragment>
                <div className="footer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-3 mb-3">
                                <a href="#">
                                    <img src="/assets/img/Logo_muadey-2.png"  className="img-fluid" />
                                </a>
                            </div>
                            <div className="col-12 col-md-3 mb-3">
                                <ul className="menu-adddress  menu-footer">
                                    <li>
                                        <h3>Phone:</h3>
                                    </li>
                                    <li>
                                        <a href="#">
                                            {/*<span className="icons-add" />*/}
                                            <span>0888.888.678</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-12 col-md-3 mb-3">
                                <ul className="menu-adddress menu-footer">
                                    <li>
                                        <h3>Email:</h3>
                                    </li>
                                    <li>
                                        <a href="#">
                                            {/*<span className="icons-phone" />*/}
                                            <span>muadey.com@gmail.com</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-12 col-md-3 mb-3">
                                <ul className="menu-adddress  menu-footer">
                                    <li>
                                        <h3>Facebook:</h3>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/muadey" target="_blank">
                                            {/*<span className="icons-mail" />*/}
                                            <span>Fanpage: Mua Đêy</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/groups/2090969451172933/" target="_blank">
                                            {/*<span className="icons-mail" />*/}
                                            <span>Group: Hội mua bán Bất Động Sản Muadey.com</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </Fragment>
        )
    }
}