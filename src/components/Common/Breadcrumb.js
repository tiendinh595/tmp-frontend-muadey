import React, {Fragment} from 'react'
import {Link} from 'react-router-dom'


export default class Breadcrumb extends React.PureComponent {

    constructor(props) {
        super(props)
    }

    render() {
        const {links} = this.props;

        return (
            <div className="breakdrum ">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <ol className="breadcrumb breakdrum-style">
                                <li className="breadcrumb-item">
                                    <a href="#">Home</a>
                                </li>
                                {
                                    links.map((item, index)=>{
                                        let class_name = index === links.length-1 ?  "breadcrumb-item active" : "breadcrumb-item";
                                        return (
                                            <li className={class_name} key={index}>
                                                <Link className="nav-link" to={item.link}>{item.label}</Link>
                                            </li>
                                        )
                                    })
                                }
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}