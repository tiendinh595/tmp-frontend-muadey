import React, {PureComponent} from 'react';
import 'Styles/loading.css'

export default class LoadingData extends PureComponent {
    render() {
        return (
            <div className="loading style-2"><div className="loading-wheel" /></div>
        )
    }
}