import React, {Fragment} from 'react'
import * as ApiCaller from 'Utils/ApiCaller'
import {serialize, getParams} from 'Utils/Helper'
import {Redirect} from 'react-router-dom'
import 'Styles/search.css'

export default class FormSearch extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            url_search: '',
            is_calling_api: false,
            is_searching: false,
            product_types: [],
            cities: [],
            districts: [],
            product_type: '',
            city_id: '',
            district_id: '',
            area: '',
            price: '',
        }
    }

    onChange = (e) => {
        const {value, name} = e.target;
        this.setState({
            [name]: value,
        });

        if (name === 'city_id') {
            ApiCaller.get(`/location/districts?city_id=${value}`)
                .then(res => {
                    if (res.code === 200) {
                        this.setState({
                            districts: res.data,
                        })
                    }
                })
        }
    };

    componentWillMount() {
        const params = getParams();
        this.setState({
            is_searching: false,
            ...params
        });
        if ('city_id' in params && params['city_id'] !== '') {
            ApiCaller.get(`/location/districts?city_id=${params['city_id']}`).then(res => {
                if (res.code === 200) {
                    this.setState({
                        districts: res.data,
                    });
                }
            });
        }
        ApiCaller.get('/location/cities').then(res => {
            if (res.code === 200) {
                this.setState({
                    cities: res.data,
                });
            }
        });

        ApiCaller.get('/product-types/').then(res => {
            if (res.code === 200) {
                this.setState({
                    product_types: res.data,
                });
            }
        });
    }

    onSearch = () => {
        let {
            product_type,
            city_id,
            district_id,
            area,
            price
        } = this.state;

        let query_string = serialize({
            product_type,
            city_id,
            district_id,
            area,
            price
        });
        console.log(query_string)
        let url_search = `/tim-kiem?${query_string}`;

        if(!this.props.is_redirect) {
            this.props.onSearch(this.state);
            window.history.pushState(null, null, url_search)
        } else {

            this.setState({
                is_searching: true,
                url_search
            });
        }
    };

    componentWillReceiveProps(newProps) {
        const params = getParams();
        this.setState({
            is_searching: false,
            ...params
        });
    }

    render() {
        const {
            cities,
            product_types,
            districts,
            is_searching,
            url_search,
            product_type,
            city_id,
            district_id,
            area,
            price
        } = this.state;

        const {is_redirect} = this.props;

        if (is_searching && is_redirect)
            return <Redirect to={url_search}/>;

        return (
            <div className="search-detail">
                <a href="javascript:;" className="icons-close"></a>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {/* Sidebar navigation */}
                            <div className="row list-chitiet">
                                <div className="col-12 col-md-6 col-lg-2">
                                    <select className="browser-default mb-3 w-100 select-search"
                                            defaultValue={product_type}
                                            name="product_type" onChange={this.onChange}>
                                        <option value="">Loại bất động sản</option>
                                        {
                                            product_types.map(type => <option value={type.id}
                                                                              key={type.id}>{type.name}</option>)
                                        }
                                    </select>
                                </div>
                                <div className="col-12 col-md-6 col-lg-2">
                                    <select className="browser-default mb-3 w-100 select-search" defaultValue={city_id}
                                            name="city_id" onChange={this.onChange}>
                                        <option value="">Tỉnh / Thành Phố</option>
                                        {
                                            cities.map(city => <option value={city.id}
                                                                       key={city.id}>{city.name}</option>)
                                        }
                                    </select>
                                </div>
                                <div className="col-12 col-md-6 col-lg-2">
                                    <select className="browser-default mb-3 w-100 select-search"
                                            defaultValue={district_id}
                                            name="district_id" onChange={this.onChange}>
                                        <option value="">Quận / Huyện</option>
                                        {
                                            districts.map(district => <option value={district.id}
                                                                              key={district.id}>{district.name}</option>)
                                        }
                                    </select>
                                </div>
                                <div className="col-12 col-md-6 col-lg-2">
                                    <select className="browser-default mb-3 w-100 select-search" defaultValue={area}
                                            name="area" onChange={this.onChange}>
                                        <option value="">Diện tích</option>
                                        <option value="30-<">{'<= 30 m2'}</option>
                                        <option value="30-50">30 - 50 m2</option>
                                        <option value="50-80">50 - 80 m2</option>
                                        <option value="80-100">80 - 100 m2</option>
                                        <option value="100-150">100 - 150 m2</option>
                                        <option value="150-200">150 - 200 m2</option>
                                        <option value="200->">{'> 200 m2'}</option>
                                    </select>
                                </div>
                                <div className="col-12 col-md-6 col-lg-2">
                                    <select className="browser-default mb-3 w-100 select-search" defaultValue={price}
                                            name="price" onChange={this.onChange}>
                                        <option value="">Mực giá</option>
                                        <option value="500000000-<">{'< 500 triệu'}</option>
                                        <option value="500000000-800000000">500 - 800 triệu</option>
                                        <option value="800000000-1000000000">800 triệu - 1 tỷ</option>
                                        <option value="1000000000-2000000000">1 tỷ - 2 tỷ</option>
                                        <option value="2000000000-3000000000">2 tỷ - 3 tỷ</option>
                                        <option value="3000000000-5000000000">3 tỷ - 5 tỷ</option>
                                        <option value="5000000000->"> {'> 5 tỷ'}</option>

                                    </select>
                                </div>
                                <div className="col-12 col-md-6 col-lg-2">
                                    <button className="btn btn-primary btn-block btn-search" onClick={this.onSearch}>
                                        <i className="fa far fa-search"/> Search
                                    </button>
                                </div>
                            </div>
                        </div>
                        {/*/. Sidebar navigation */}
                    </div>
                </div>
            </div>
        )
    }
}