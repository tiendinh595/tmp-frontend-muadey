import React, {Fragment} from 'react'
import {observer,inject} from 'mobx-react'
import MasterLayout from 'Components/Layout/MasterLayout'
import {Link} from 'react-router-dom'
import {NotificationContainer} from 'react-notifications';
import 'Styles/user.css'

@inject('commonStore', 'userStore')
@observer
export default class UserLayout extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <MasterLayout>
                <NotificationContainer/>
                <div className="nhadatban_content nhadatban__canhan pb-4">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="info-canhan">
                                    <h3 className="info-canhan__title">
                                        Trang cá nhân
                                    </h3>
                                    <div className="info-canhan__img">
                                        <img src={this.props.userStore.current_user.avatar} />
                                        <p className="name-user">{this.props.userStore.current_user.name}</p>
                                        <p><Link to="/logout" style={{color: 'red'}}>đăng xuất</Link></p>
                                    </div>
                                    <div className="info-canhan__title2">
                                        Quản lý thông tin cá nhân
                                    </div>
                                    <div className="info-canhan__list">
                                        <ul>
                                            <li>
                                                <Link to="/profile"> Thay đổi thông tin cá nhân</Link>
                                            </li>
                                            <li>
                                                <Link to="/doi-mat-khau"> Thay đổi mật khẩu</Link>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="info-canhan__title2">
                                        Quản lý tin rao
                                    </div>
                                    <div className="info-canhan__list">
                                        <ul>
                                            <li>
                                                <Link to="/tin-rao" title="Quản lý tin rao bán/cho thuê"> Quản lý tin rao </Link>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="info-canhan__title2">
                                        Hướng dẫn
                                    </div>
                                    <div className="info-canhan__list">
                                        <ul>
                                            <li>
                                                <a href="#" title="Hướng dẫn sử dụng"> Hướng dẫn sử dụng </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            { this.props.children }

                        </div>
                    </div>
                </div>

            </MasterLayout>
        )
    }
}