import React, {Fragment} from 'react'
import {inject, observer} from 'mobx-react'
import WindowTitle from 'Components/Layout/WindowTitle'
import MasterLayout from 'Components/Layout/MasterLayout'
import * as ApiCaller from 'Utils/ApiCaller'
import {withRouter} from 'react-router-dom'
import 'Styles/form.css'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import BaseComponent from 'Components/BaseComponent'
import Recaptcha from 'react-grecaptcha';


@inject('profileStore', 'commonStore', 'userStore')
@observer
export default class Register extends BaseComponent {
    constructor(props) {
        super(props);
        this.initState();
    }

    initState = () => {
        this.state = {
            is_calling_api: false,
            cities: [],
            districts: [],
            wards: [],
            errors: [],
            name: null,
            username: null,
            password: null,
            re_password: null,
            email: null,
            mobile: null,
            birthday: null,
            gender: 1,
            account_type: 1,
            city_id: null,
            district_id: null,
            ward_id: null,
            captcha: ''
        }
    };

    onChange = (e) => {
        const {value, name} = e.target;
        this.setState({
            [name]: value,
        });

        if (name === 'city_id') {
            ApiCaller.get(`/location/districts?city_id=${value}`)
                .then(res => {
                    if (res.code === 200) {
                        this.setState({
                            districts: res.data,
                        })
                    }
                })
        } else if (name === 'district_id') {
            ApiCaller.get(`/location/wards?district_id=${value}`)
                .then(res => {
                    if (res.code === 200) {
                        this.setState({
                            wards: res.data,
                        })
                    }
                })
        }
    };

    componentWillMount() {
        ApiCaller.get('/location/cities')
            .then(res => {
                if (res.code === 200) {
                    this.setState({
                        cities: res.data,
                    });
                }
            })
    }

    componentDidMount() {

    }

    onRegister = (e) => {
        e.preventDefault();
        const {name, username, password, re_password, email, mobile, birthday, gender, account_type, city_id, district_id, ward_id, captcha} = this.state;

        this.setState({
            is_calling_api: true
        });

        ApiCaller.post('/register', {
            name,
            username,
            password,
            re_password,
            email,
            mobile,
            birthday,
            gender,
            account_type,
            city_id,
            district_id,
            ward_id,
            captcha
        })
            .then(res => {
                this.setState({
                    is_calling_api: false
                });
                if (res.code !== 200) {
                    NotificationManager.error(res.message);
                    this.setState({
                        errors: res.errors
                    })
                } else  {
                    NotificationManager.success(res.message);
                    this.initState();
                    this.props.commonStore.setToken(res.data.token);
                    this.props.userStore.setCurrentUser(res.data);
                    window.location.href = '/dang-tin-ban-nha-dat';
                }

            })
            .catch(err=>{
                this.setState({
                    is_calling_api: false
                });
                NotificationManager.error('Có lỗi xảy ra');

            })
    };

    render() {
        const {errors} = this.state;
        console.log('app_loaded', this.props.commonStore.app_loaded);

        return (
            <MasterLayout>
                <NotificationContainer/>
                <WindowTitle title="Đăng ký"/>
                <div className="nhadatban_content">

                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-9">
                                <div className="box-register mt-5">
                                    <div className="info_up mt-5">
                                        <h4 className="info_up__h">Đăng ký tài khoản</h4>
                                        <form onSubmit={this.onRegister}>
                                            <div className="form-group row mb-1">
                                                {/* Material input */}
                                                <label htmlFor="dtdd" className="col-sm-3 col-form-label label-dangtin">Điện
                                                    thoại di động (
                                                    <span className="canhbao">*</span>)</label>
                                                <div className="col-sm-9">
                                                    <div className="md-form mt-0">
                                                        <input type="text" id="dtdd"
                                                               className="form-control white border-color" name="mobile"
                                                               onChange={this.onChange}/>
                                                    </div>
                                                    {this.showErrors(errors, 'mobile')}
                                                </div>
                                            </div>
                                            <div className="form-group row mb-1">
                                                {/* Material input */}
                                                <label htmlFor="fullname"
                                                       className="col-sm-3 col-form-label label-dangtin">Tên đầy đủ (
                                                    <span className="canhbao">*</span>)</label>
                                                <div className="col-sm-9">
                                                    <div className="md-form mt-0">
                                                        <input type="text" id="fullname"
                                                               className="form-control white border-color" name="name"
                                                               onChange={this.onChange}/>
                                                    </div>
                                                    {this.showErrors(errors, 'name')}
                                                </div>
                                            </div>
                                            <div className="form-group row mb-1">
                                                {/* Material input */}
                                                <label htmlFor="matkhau"
                                                       className="col-sm-3 col-form-label label-dangtin">Mật khẩu (
                                                    <span className="canhbao">*</span>)</label>
                                                <div className="col-sm-9">
                                                    <div className="md-form mt-0">
                                                        <input type="password" id="matkhau"
                                                               className="form-control white border-color"
                                                               name="password" onChange={this.onChange}/>
                                                    </div>
                                                    {this.showErrors(errors, 'password')}
                                                </div>
                                            </div>
                                            <div className="form-group row mb-1">
                                                {/* Material input */}
                                                <label htmlFor="rematkhau"
                                                       className="col-sm-3 col-form-label label-dangtin">Nhập lại mật
                                                    khẩu (
                                                    <span className="canhbao">*</span>)</label>
                                                <div className="col-sm-9">
                                                    <div className="md-form mt-0">
                                                        <input type="password" id="rematkhau"
                                                               className="form-control white border-color"
                                                               name="re_password" onChange={this.onChange}/>
                                                    </div>
                                                    {this.showErrors(errors, 're_password')}

                                                </div>
                                            </div>

                                            <div className="form-group row mb-1 mt-3">
                                                {/* Material input */}
                                                <label htmlFor="phuongxa"
                                                       className="col-sm-3 col-form-label label-dangtin">Mã bảo
                                                    vệ </label>
                                                <div className="col-sm-9">
                                                    <div className="form-group row">
                                                        {/* Material input */}
                                                        <div className="col-sm-9">
                                                            <div className="md-form mt-0">


                                                                <Recaptcha
                                                                    sitekey="6LfJL1QUAAAAABBEzplHUGtJw8qp01lTqNmfoL80"
                                                                    callback={res=>this.setState({"captcha": res})}
                                                                    expiredCallback={()=>{}}
                                                                    className="customClassName"
                                                                />
                                                                {this.showErrors(errors, 'captcha')}


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group row mt-2">
                                                {/* Material input */}
                                                <label htmlFor="dangky"
                                                       className="col-sm-3 col-form-label label-dangtin"/>
                                                <div className="col-sm-9">
                                                    <div className="md-form mt-0">
                                                        <button type="submit"
                                                                className={this.state.is_calling_api ? "btn btn-primary btn-dang waves-effect waves-light btn-block disabled" : "btn btn-primary btn-dang waves-effect waves-light btn-block"}>
                                                            <span className="icons-dang"/> Đăng ký
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group row mt-3">
                                                {/* Material input */}
                                                <label htmlFor="dangky"
                                                       className="col-sm-3 col-form-label label-dangtin"/>
                                                <div className="col-sm-9">
                                                    <div className="md-form mt-0 text-left fs13">
                                                        <strong> Chú ý: thông tin Tên đăng nhập, email, số điện thoại di
                                                            động không thể thay đổi sau khi đăng ký.</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="col-4">
                            </div>
                        </div>
                    </div>
                </div>


            </MasterLayout>
        )
    }
}