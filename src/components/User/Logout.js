import React, {Fragment} from 'react'
import {inject, observer} from 'mobx-react'
import WindowTitle from 'Components/Layout/WindowTitle'
import MasterLayout from 'Components/Layout/MasterLayout'
import BaseComponent from 'Components/BaseComponent'
import * as ApiCaller from 'Utils/ApiCaller'
import {NotificationContainer, NotificationManager} from 'react-notifications';


@inject('userStore', 'authStore')
@observer
export default class Logout extends BaseComponent {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.authStore.logout();
    }

    render() {
        return null;
    }
}