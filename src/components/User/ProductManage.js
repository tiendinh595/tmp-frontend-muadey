import React, {Component, PureComponent} from 'react'
import UserLayout from 'Components/Layout/UserLayout'
import WindowTitle from 'Components/Layout/WindowTitle'
import * as ApiCaller from 'Utils/ApiCaller'
import BaseComponent from "Components/BaseComponent";
import {NotificationManager} from 'react-notifications';
import ReactPaginate from 'react-paginate';
import swal from 'sweetalert';
import LoadingSpinner from 'loading-spinner-reactjs'
import {Link} from 'react-router-dom'

class ProductItem extends PureComponent {
    constructor(props) {
        super(props)
    }

    onDelete = () => {
        this.props.onDelete(this.props.product.id)
    };

    render() {
        const {product} = this.props;
        return (
            <tr>
                <td>{product.id}</td>
                <td><Link to={`/product/${product.id}/${product.slug}`}>{product.title}</Link></td>
                <td>{product.created_at}</td>
                <td>
                    <button className="btn btn-sm btn-danger" onClick={this.onDelete}><i
                        className="fa fa-trash-o white-text"/></button>
                    <Link to={`/cap-nhat-tin-ban-nha-dat/${product.id}`} className="btn btn-sm btn-success"><i
                        className="fa fa-edit white-text"/></Link>
                </td>
            </tr>
        )
    }
}

export default class ProductManage extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            is_loading: false,
            errors: [],
            old_password: '',
            new_password: '',
            re_new_password: '',
            product_types: [],
            products: [],
            meta_data: {},
            limit: 10,
            page: 1,
            product_type_master_id: '',
            product_type_child_id: '',
            title: ''
        }
    }

    onChange = (e) => {
        e.preventDefault();
        let {value, name} = e.target;

        switch (name) {
            case 'product_type_child_id':
                const product_type_master_id = parseInt(e.target.options[e.target.selectedIndex].getAttribute('master'));
                if (product_type_master_id === 0) {
                    this.setState({
                        product_type_master_id: value,
                        product_type_child_id: ''
                    });
                } else {
                    this.setState({
                        product_type_master_id: product_type_master_id,
                        product_type_child_id: value
                    });
                }

                break;
            default:
                this.setState({
                    [name]: value,
                });
                break;
        }

    };

    genUrl = () => {
        const {title, product_type_master_id, product_type_child_id, limit, page} = this.state;
        let url = `/products/?only_me=true&limit=${limit}&page=${page}`;
        if (title.trim() !== '')
            url += `&title=${title}`;
        if (product_type_master_id !== '')
            url += `&product_type_master_id=${product_type_master_id}`;
        if (product_type_child_id !== '')
            url += `&product_type_child_id=${product_type_child_id}`;
        return url;
    };

    onSearch = (e) => {
        e.preventDefault();
        this.loadProducts();
    };

    componentWillMount() {

        ApiCaller.get('/product-types/')
            .then(res => {
                if (res.code === 200) {
                    this.setState({
                        product_types: res.data,
                    });
                }
            });
        this.loadProducts(`/products/?only_me=true&limit=${this.state.limit}`)
    }

    loadProducts = () => {
        this.setState({
            is_loading: true
        });

        ApiCaller.get(this.genUrl())
            .then(res => {
                this.setState({
                    products: res.data,
                    meta_data: res.metadata
                })
            })
            .finally(() => this.setState({is_loading: false}))
    };

    handlePageClick = (data) => {
        this.setState({
            page: data.selected + 1
        });
        this.loadProducts();
    };


    onDeleteProduct = (product_id) => {
        swal({
            title: "Bạn có muốn xóa?",
            text: "Bạn có chắc chắn muốn xóa bài đăng này",
            icon: "warning",
            buttons: ['hủy', 'xóa'],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ApiCaller.remove(`/products/?pk=${product_id}`)
                        .then(res => {
                            if (res.code === 200) {
                                this.setState({
                                    products: this.state.products.filter(product => product.id !== product_id)
                                });
                                swal(res.message, {
                                    icon: "success",
                                });
                            } else {
                                this.setState({
                                    products: this.state.products.filter(product => product.id !== product_id)
                                });
                                swal(res.message, {
                                    icon: "error",
                                });
                            }
                        });

                }
            });
    };

    render() {
        const {is_loading, products} = this.state;

        return (
            <UserLayout>
                <WindowTitle title="Quản lý tin rao"/>
                <div className="col-md-8">
                    <h3 className="info-canhan__title left">
                        Quản lý tin rao
                    </h3>
                    <form className="mt-3">
                        <div className="row">

                            <div className="col-sm-6 ">
                                <label className="label-matin">Loại bất động sản</label>
                                <div className="md-form mt-0">
                                    <div className="select_wrapper">
                                        <select
                                            className="browser-default mb-3 w-100 select-search select_box"
                                            name="product_type_child_id" onChange={this.onChange}
                                            defaultValue={0}>
                                            <option value="0">Chọn loại</option>
                                            {
                                                this.state.product_types.map((type) => {
                                                    let result = [];
                                                    result.push(<option
                                                        value={type.id}
                                                        key={type.id}
                                                        master="0">{type.name}</option>);
                                                    type.childs.map(child => {
                                                        result.push(<option value={child.id}
                                                                            key={child.id}
                                                                            master={type.id}>
                                                            -- {child.name}</option>)
                                                    });

                                                    return result;
                                                })
                                            }
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 ">
                                <label className="label-matin">Tiêu đề</label>
                                <div className="md-form mt-0">
                                    <input type="text" className="form-control  white border-color"
                                           placeholder="nhập tiêu đề bài đăng " name="title" onChange={this.onChange}/>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="md-form mt-0">
                                    <p className="wapper-submit text-center">
                                        <button
                                            type="submit"
                                            className="btn btn-primary btn-dang waves-effect waves-light ml-0"
                                            onClick={this.onSearch}
                                        >
                                            <span className="fa fa-search white_serch"/> Tìm kiếm
                                        </button>
                                    </p>
                                </div>

                            </div>
                        </div>
                    </form>
                    <div className="table-responsive table-canhan mt-4">
                        <table className="table table-bordered">
                            {/*Table head*/}
                            <thead>
                            <tr className="text-uppercase">
                                <th>ID</th>
                                <th width="35%">Tiêu Đề</th>
                                <th>Ngày Đăng</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            {
                                is_loading
                                    ? null
                                    : products.map((product, index) => <ProductItem key={index} product={product}
                                                                                    onDelete={this.onDeleteProduct}/>)
                            }


                            </tbody>
                        </table>

                        {
                            is_loading
                                ? <LoadingSpinner/>
                                : null
                        }

                        {
                            (() => {
                                if (products.length !== 0) {
                                    return (
                                        <div className="row">
                                            <div className="pagination_cate text-center w-100">
                                                <nav aria-label="pagination ">
                                                    <ReactPaginate previousLabel={"«"}
                                                                   previousClassName="page-item"
                                                                   nextClassName="page-item"
                                                                   previousLinkClassName="page-link"
                                                                   nextLinkClassName="page-link"
                                                                   nextLabel={"»"}
                                                                   breakLabel={<a href="">...</a>}
                                                                   breakClassName={"break-me"}
                                                                   pageCount={this.state.meta_data.total_pages}
                                                                   marginPagesDisplayed={2}
                                                                   pageRangeDisplayed={5}
                                                                   onPageChange={this.handlePageClick}
                                                                   containerClassName={"pagination justify-content-center"}
                                                                   subContainerClassName={"pages pagination"}
                                                                   activeClassName={"active"}
                                                                   pageClassName="page-item"
                                                                   pageLinkClassName="page-link"
                                                    />
                                                </nav>
                                            </div>
                                        </div>
                                    )
                                }
                            })()
                        }

                    </div>
                </div>
            </UserLayout>
        )
    }
}