import React, {Component} from 'react'
import UserLayout from 'Components/Layout/UserLayout'
import WindowTitle from 'Components/Layout/WindowTitle'
import {observer, inject} from 'mobx-react'
import * as ApiCaller from 'Utils/ApiCaller'
import BaseComponent from "Components/BaseComponent";
import {NotificationManager} from 'react-notifications';
import {API_URI} from 'Constants/index'


@inject('userStore')
@observer
export default class Profile extends BaseComponent {

    constructor(props) {
        super(props);
        this.initState();
    }

    initState = () => {
        const {current_user} = this.props.userStore;
        this.state = {
            is_calling_api: false,
            is_uploading: false,
            cities: [],
            districts: [],
            wards: [],
            errors: [],
            name: current_user.name,
            email: current_user.email,
            mobile: current_user.mobile,
            birthday: current_user.birthday,
            gender: current_user.gender,
            city_id: current_user.city ? current_user.city.id : '',
            district_id: current_user.district ? current_user.district.id : '',
            ward_id: current_user.ward ? current_user.ward.id : '',
            address: current_user.address,
            facebook: current_user.facebook,
            avatar: current_user.avatar.replace(`${API_URI}/`, ''),
            avatar_obj: this.getDefaultAvatarObj(current_user)
        }
    };

    getDefaultAvatarObj = (current_user) => {
        return {
            id: 0,
            url: current_user.avatar.replace(`${API_URI}/`, ''),
            full_url: current_user.avatar
        }
    };

    onResetAvatar = (e) => {
        e.preventDefault();
        const {current_user} = this.props.userStore;
        this.setState({
            avatar_obj: this.getDefaultAvatarObj(current_user),
            avatar: current_user.avatar.replace(`${API_URI}/`, '')
        })
    };

    onUploadFile = (e) => {
        const file = e.target.files[0];
        this.setState({
            is_uploading: true
        });
        ApiCaller.post('/files/', {file}, (progressEvent) => {
            let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
        }).then(res => {
            console.log(res)
            if (res.code === 200) {
                this.setState({
                    avatar_obj: res.data,
                    avatar: res.data.url
                })
            } else {
                NotificationManager.error(res.message)
            }
        })
            .finally(()=>{
                this.setState({
                    is_uploading: false
                });
            })
    };

    onChange = (e) => {
        e.preventDefault();
        const {value, name} = e.target;
        this.setState({
            [name]: value,
        });


        switch (name) {
            case 'city_id' :
                ApiCaller.get(`/location/districts?city_id=${value}`)
                    .then(res => {
                        if (res.code === 200) {
                            this.setState({
                                districts: res.data,
                            })
                        }
                    });
                break;
            case 'district_id':
                ApiCaller.get(`/location/wards?district_id=${value}`)
                    .then(res => {
                        if (res.code === 200) {
                            this.setState({
                                wards: res.data,
                            })
                        }
                    });
                break;
        }
    };

    async componentWillMount() {
        const {current_user} = this.props.userStore;

        const cities = await ApiCaller.get('/location/cities');
        if (cities.code === 200) {
            this.setState({
                cities: cities.data
            });
            if (current_user.city !== '') {
                const districts = await ApiCaller.get(`/location/districts?city_id=${current_user.city.id}`);
                if (districts.code === 200) {
                    this.setState({
                        districts: districts.data
                    });
                    if (current_user.district !== '') {
                        const wards = await ApiCaller.get(`/location/wards?district_id=${current_user.district.id}`);
                        if (wards.code === 200) {
                            this.setState({
                                wards: wards.data
                            });
                        }
                    }

                }
            }

        }
        this.setState({
            is_loading: false
        })
    }

    componentDidMount() {
        jQuery("#danganh1").click(function (event) {

            jQuery("#choosefile1").click();
        });
    }

    onUpdateProfile = (e) => {
        e.preventDefault();
        this.setState({
            is_calling_api: true,
            errors: {}
        });
        const {name, email, mobile, birthday, gender, city_id, district_id, ward_id, address, captcha, facebook, avatar} = this.state;
        ApiCaller.put('/profile', {
            name,
            email,
            mobile,
            birthday,
            gender,
            city_id,
            district_id,
            ward_id,
            address,
            captcha,
            facebook,
            avatar
        })
            .then(res => {
                if (res.code !== 200) {
                    NotificationManager.error(res.message);
                    this.setState({
                        errors: res.errors
                    })
                } else {
                    NotificationManager.success(res.message);
                    this.props.userStore.setCurrentUser(res.data);
                }
            })
            .finally(() => {
                this.setState({
                    is_calling_api: false
                });
            })
    };

    render() {
        const {current_user} = this.props.userStore;
        const {errors, avatar_obj, is_uploading} = this.state;

        return (
            <UserLayout>

                <WindowTitle title="Cập nhật thông tin cá nhân"/>
                <div className="col-md-8">
                    <h3 className="info-canhan__title left">
                        Thay đổi thông tin cá nhân
                    </h3>
                    <form className="mt-3" onSubmit={this.onUpdateProfile}>
                        <h4 className="info_up__h private">Thông tin cá nhân</h4>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="hoten"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right"> Họ và
                                tên (
                                <span className="canhbao">*</span>) </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <input type="text" id="hoten" className="form-control white border-color"
                                           name="name" onChange={this.onChange} defaultValue={current_user.name || ''}/>
                                </div>
                                {this.showErrors(errors, 'name')}
                            </div>
                        </div>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="ngaysinh"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right">Ngày
                                sinh </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <input type="date" id="ngaysinh"
                                           className="form-control ngaysinh white border-color" name="birthday"
                                           onChange={this.onChange} defaultValue={current_user.birthday || ''}/>
                                </div>
                                {this.showErrors(errors, 'birthday')}

                            </div>
                        </div>
                        <div className="form-group row" style={{marginTop: '-10px'}}>
                            {/* Material input */}
                            <label className="col-sm-3 col-form-label label-dangtin label-info-user text-right"/>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    {/* Default checkbox */}
                                    <div className="form-check mr-2 mb-2 float-left p-0">
                                        <input className="filled-in form-check-input"
                                               type="radio" id="checknam"
                                               defaultChecked={current_user.gender === 1}
                                               name="gender" value={1} onChange={this.onChange}/>
                                        <label className="form-check-label"
                                               htmlFor="checknam">Nam</label>
                                    </div>
                                    {/* Default checkbox */}
                                    <div className="form-check mr-2 mb-2 float-left pl-0">
                                        <input className="filled-in form-check-input"
                                               defaultChecked={current_user.gender === 2}
                                               type="radio" id="checknu" name="gender" value={2}
                                               onChange={this.onChange}/>
                                        <label className="form-check-label fillDesgin"
                                               htmlFor="checknu">Nữ</label>
                                    </div>
                                </div>
                                {this.showErrors(errors, 'gender')}

                            </div>
                        </div>
                        <div className="form-group row mb-1 mt-3">
                            {/* Material input */}
                            <label htmlFor="tinhthanh"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right">Tỉnh /
                                Thành </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <div className="select_wrapper">

                                        <select
                                            className="browser-default mb-3 w-100 select-search select_box"
                                            name="city_id" onChange={this.onChange}
                                            defaultValue={current_user.city ? current_user.city.id : ''}
                                        >

                                            <option value={0}>Tỉnh / Thành</option>
                                            {
                                                this.state.cities.map((city) => <option
                                                    value={city.id}
                                                    key={city.id}
                                                    selected={(current_user.city ? current_user.city.id : '') === city.id}>{city.name}</option>)
                                            }
                                        </select>
                                    </div>
                                </div>
                                {this.showErrors(errors, 'city_id')}

                            </div>
                        </div>
                        <div className="form-group row mb-1 mt-3">
                            {/* Material input */}
                            <label htmlFor="quanhuyen"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right">Quận /
                                Huyện </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <select
                                        className="browser-default mb-3 w-100 select-search select_box"
                                        name="district_id" onChange={this.onChange}
                                        defaultValue={current_user.district ? current_user.district.id : ''}>
                                        <option value={0}>Huyện / Quận</option>

                                        {
                                            this.state.districts.map((district) => <option
                                                value={district.id}
                                                key={district.id}
                                                selected={(current_user.district ? current_user.district.id : '') === district.id}>{district.name}</option>)
                                        }
                                    </select>
                                </div>
                                {this.showErrors(errors, 'district_id')}

                            </div>
                        </div>
                        <div className="form-group row mb-1 mt-3">
                            {/* Material input */}
                            <label htmlFor="phuongxa"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right">Phường /
                                Xã </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <select
                                        className="browser-default mb-3 w-100 select-search select_box"
                                        name="ward_id" onChange={this.onChange}
                                        defaultValue={current_user.ward ? current_user.ward.id : ''}>
                                        <option value="0">Xã / Phường</option>
                                        {
                                            this.state.wards.map((ward) => <option
                                                value={ward.id}
                                                key={ward.id}
                                                selected={(current_user.ward ? current_user.ward.id : '') === ward.id}>{ward.name}</option>)
                                        }
                                    </select>
                                </div>
                                {this.showErrors(errors, 'ward_id')}

                            </div>
                        </div>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="diachi"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right">Địa
                                chỉ </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <input type="text" id="diachi" className="form-control white border-color"
                                           name="address" onChange={this.onChange}
                                           defaultValue={current_user.address || ''}/>
                                </div>
                                {this.showErrors(errors, 'address')}

                            </div>
                        </div>
                        <h4 className="info_up__h private">Thông tin liên hệ</h4>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="mobile"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right">Mobile </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <input type="text" id="mobile" className="form-control white border-color"
                                           name="mobile" onChange={this.onChange}
                                           defaultValue={current_user.mobile || ''} readOnly={true}/>
                                </div>
                                {this.showErrors(errors, 'mobile')}

                            </div>
                        </div>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="email"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right">Email (
                                <span className="canhbao">*</span>) </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <input type="email" id="email" className="form-control white border-color"
                                           name="email" onChange={this.onChange}
                                           defaultValue={current_user.email || ''}/>
                                </div>
                            </div>
                            {this.showErrors(errors, 'email')}

                        </div>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="facebook"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right">Facebook </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <input type="text" id="facebook" className="form-control white border-color"
                                           name="facebook" onChange={this.onChange}
                                           defaultValue={current_user.facebook || ''}/>
                                </div>
                                {this.showErrors(errors, 'facebook')}

                            </div>
                        </div>

                        <h4 className="info_up__h private">avatar</h4>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="tieu_de"
                                   className="col-sm-3 col-form-label label-dangtin text-right label-info-user">Ảnh</label>
                            <div className="col-sm-9 text-left">
                                <div className="md-form mt-0">
                                    <div className="form_up__input input_file">
                                        <input className="hidden" type="file" name="blah1" id="choosefile1"
                                               accept="image/*" onChange={this.onUploadFile} />
                                        <a id="danganh1" className="icons-add_upload" href="javascript:;"
                                           title="Đăng ảnh">
                                        </a>
                                        {
                                            is_uploading
                                                ? <div className="progress primary-color-dark">
                                                    <div className="indeterminate"/>
                                                </div>
                                                : null
                                        }
                                        <p>
                                            ( File hỗ trợ các định dạng jpg, png, gif, jpeg )
                                        </p>
                                        <div className="showhide-img_item" style={{display: 'block', marginLeft: 0}}>
                                            <img id="blah1" className="blah"
                                                 src={avatar_obj.full_url}/>
                                            <a href="" onClick={this.onResetAvatar} className="removeLink"/>
                                        </div>

                                    </div>
                                </div>
                                {this.showErrors(errors, 'avatar')}

                            </div>
                        </div>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="viber"
                                   className="col-sm-3 col-form-label label-dangtin label-info-user text-right"> </label>
                            <div className="col-sm-9">
                                <div className="md-form mt-0">
                                    <button type="submit"
                                            className="btn btn-primary btn-dang waves-effect waves-light ml-0">
                                        Lưu lại
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </UserLayout>
        )
    }
}