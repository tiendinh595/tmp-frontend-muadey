import React, {Component} from 'react'
import UserLayout from 'Components/Layout/UserLayout'
import WindowTitle from 'Components/Layout/WindowTitle'
import {observer, inject} from 'mobx-react'
import * as ApiCaller from 'Utils/ApiCaller'
import BaseComponent from "Components/BaseComponent";
import {NotificationManager} from 'react-notifications';

export default class ChangePassword extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            is_calling_api: false,
            errors: [],
            old_password: '',
            new_password: '',
            re_new_password: ''
        }
    }

    resetState = () => {
        this.setState({
            is_calling_api: false,
            errors: [],
            old_password: '',
            new_password: '',
            re_new_password: ''
        });
    };

    onChange = (e) => {
        e.preventDefault();
        const {value, name} = e.target;
        this.setState({
            [name]: value,
        });
    };


    onChangePassword = (e) => {
        e.preventDefault();
        this.setState({
            is_calling_api: true,
            errors: {}
        });
        const {old_password, new_password, re_new_password} = this.state;
        ApiCaller.put('/change-password', {old_password, new_password, re_new_password})
            .then(res => {
                if (res.code !== 200) {
                    NotificationManager.error(res.message);
                    this.setState({
                        errors: res.errors
                    })
                } else {
                    NotificationManager.success(res.message);
                    this.resetState()
                }
            })
            .finally(() => {
                this.setState({
                    is_calling_api: false
                });
            })
    };

    render() {
        const {is_calling_api, errors, old_password, new_password, re_new_password} = this.state;
        return (
            <UserLayout>
                <WindowTitle title="Thay đổi mật khẩu"/>
                <div className="col-md-8">
                    <h3 className="info-canhan__title left">
                        Thay đổi mật khẩu
                    </h3>
                    <form className="mt-3">
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="diachi" className="col-sm-3 col-form-label label-dangtin text-center"> Mật
                                khẩu cũ</label>
                            <div className="col-sm-6">
                                <div className="md-form mt-0">
                                    <input type="password" className="form-control white border-color"
                                           name="old_password" onChange={this.onChange} value={old_password}/>
                                </div>
                                {this.showErrors(errors, 'old_password')}
                                <p className="text-left">
                                    Nếu bạn quên mật khẩu hoặc đăng nhập bằng Facebook, Google mà không cần đăng ký thì
                                    hãy thoát khỏi tài khoản, sau đó sử
                                    dụng chức năng lấy lại mật khẩu để lấy mật khẩu hiện tại.
                                </p>
                            </div>
                        </div>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="diachi" className="col-sm-3 col-form-label label-dangtin text-center"> Mật
                                khẩu mới</label>
                            <div className="col-sm-6">
                                <div className="md-form mt-0">
                                    <input type="password" className="form-control white border-color"
                                           name="new_password" onChange={this.onChange} value={new_password}/>
                                </div>
                                {this.showErrors(errors, 'new_password')}

                            </div>
                        </div>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="diachi" className="col-sm-3 col-form-label label-dangtin text-center"> Gõ
                                lại mật khẩu</label>
                            <div className="col-sm-6">
                                <div className="md-form mt-0">
                                    <input type="password" className="form-control white border-color"
                                           name="re_new_password" onChange={this.onChange} value={re_new_password}/>
                                </div>
                                {this.showErrors(errors, 're_new_password')}

                            </div>
                        </div>
                        <div className="form-group row">
                            {/* Material input */}
                            <label htmlFor="diachi"
                                   className="col-sm-3 col-form-label label-dangtin text-center"> </label>
                            <div className="col-sm-6">
                                <div className="md-form mt-0">
                                    <button type="submit"
                                            className="btn btn-primary btn-dang waves-effect waves-light ml-0"
                                            onClick={this.onChangePassword}
                                            disabled={is_calling_api}
                                    >
                                        Lưu lại
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </UserLayout>
        )
    }
}