import React from 'react'
import ReactDOM, {render, hydrate} from 'react-dom'
import 'babel-polyfill';
import {Provider} from 'mobx-react'
import App from 'Components/App'
import {
    userStore,
    profileStore,
    authStore,
    commonStore
} from 'Stores'

const stores = {
    userStore,
    profileStore,
    authStore,
    commonStore
};

window._____APP_STATE_____ = stores;
const rootElement = document.getElementById('root');
if (rootElement.hasChildNodes()) {
    hydrate(<Provider {...stores}>
        <App />
    </Provider>, rootElement);
} else {
    render(<Provider {...stores}>
        <App />
    </Provider>, rootElement);
}
