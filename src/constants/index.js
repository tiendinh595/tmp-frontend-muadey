/**
 * Created by dinh on 1/11/18.
 */
let NODE_ENV = process.env.NODE_ENV;
export const API_URI = NODE_ENV === 'production' ? 'https://api.muadey.com' : 'http://127.0.0.1:8000';
